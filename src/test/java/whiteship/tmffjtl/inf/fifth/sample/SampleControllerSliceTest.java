package whiteship.tmffjtl.inf.fifth.sample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import lombok.extern.slf4j.Slf4j;

/**
 * slice test : 해당 컨트롤러만 테스트 하는 방법 
 * */
@RunWith(SpringRunner.class)
@WebMvcTest(SampleController.class)
public class SampleControllerSliceTest {
	
	// 콘솔에 찍히는 모든걸 캡처한다. junit에 있는 모듈을 스프링이 갖다씀.
	@Rule
	public OutputCapture outputCapture = new OutputCapture();
	
	@MockBean
	SampleService mockSampleService;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void hello() throws Exception {
		when(mockSampleService.getName()).thenReturn("whiteship");
		
		mockMvc.perform(get("/hello"))
			.andExpect(content().string("hello whiteship"));
		
		// 모든 콘솔에 찍히는 로그를 확인하는 방법.. 
		assertThat(outputCapture.toString())
			.contains("holoman")
			.contains("skip");
	}
}
