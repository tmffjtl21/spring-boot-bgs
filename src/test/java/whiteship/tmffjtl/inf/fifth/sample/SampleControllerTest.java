package whiteship.tmffjtl.inf.fifth.sample;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)		// Mock MVC를 사용한다? 
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SampleControllerTest {
	
	// MOCK Test로 사용 시작 
//	@Autowired
//	MockMvc mockMvc;
//	
//	@Test
//	public void hello() throws Exception{
//		mockMvc.perform(get("/hello"))
//				.andExpect(status().isOk())
//				.andExpect(content().string("hello taejin"))
//				.andDo(print());
//	}
	// MOCK Test로 사용 끝
	
	// Random_port로 테스트 시작 
	@Autowired
	WebTestClient webTestClient;
	
	// sampleService를 mockBean으로 만들어서 교체한다. 
	@MockBean
	SampleService mockSampleService;
	
	@Test
	public void hello() throws Exception {
		when(mockSampleService.getName()).thenReturn("whiteship");
		
		webTestClient.get().uri("/hello").exchange()
			.expectStatus().isOk()
			.expectBody(String.class).isEqualTo("hello whiteship");
	}
	// Random_port로 테스트 끝
}
