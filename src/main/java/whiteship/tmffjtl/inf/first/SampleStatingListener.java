package whiteship.tmffjtl.inf.first;

import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;

import lombok.extern.slf4j.Slf4j;

// 이건 어플리케이션 제일 처음에 실행되는데.. ApplicationContext 가 만들어지기도 전에 실행되는 이벤트라서 따로 run 이전에 지정해줘야 한다.
public class SampleStatingListener implements ApplicationListener<ApplicationStartingEvent>{

	@Override
	public void onApplicationEvent(ApplicationStartingEvent event) {
//		log.info("=============");
//		log.info("Application Starting");
//		log.info("=============");
		
		System.out.println("=============");
		System.out.println("Application Starting");
		System.out.println("=============");
	}
}
