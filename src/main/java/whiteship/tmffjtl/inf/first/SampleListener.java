package whiteship.tmffjtl.inf.first;

import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

@Component
public class SampleListener {
	public SampleListener(ApplicationArguments arguments) {
		System.out.println("programArgs : " + arguments.containsOption("programArgs"));
		System.out.println("vmArgs : " + arguments.containsOption("vmArgs"));
	}
}
