package whiteship.tmffjtl.inf.first;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

/**
 * SpringApplication 강의 1~2부 
 * */

@SpringBootApplication
@Slf4j
public class WhiteshipApplication implements ApplicationRunner{

	public static void main(String[] args) {
//		SpringApplication app = new SpringApplication(WhiteshipApplication.class);
//		app.setBannerMode(Banner.Mode.OFF);
//		app.run(args);
		
//		new SpringApplicationBuilder()
//		.sources(WhiteshipApplication.class)
//		.run(args);
		
//		SpringApplication app = new SpringApplication(WhiteshipApplication.class);
//		app.addListeners(new SampleStatingListener());
//		app.run(args);
		
		SpringApplication app = new SpringApplication(WhiteshipApplication.class);
		// 클래스패스에 webmvc와 리액티브가 둘다 있다면 서블릿이 항상 우선이지만 리액티브로 쓰고싶다면 선언해야된다.
		app.setWebApplicationType(WebApplicationType.REACTIVE);		 
		app.run(args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// ApplicationRunner 를 상속받으면 Application이 실행될때 같이 실행가능 @Order로 옵션을 줘서 순서를 지정 가능 ( 1 ) 낮을수록 우선 실행 
		// listener에도 등록 가능하다.
	}
}
