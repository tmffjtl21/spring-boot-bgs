package whiteship.tmffjtl.inf.first;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SampleStartedListener implements ApplicationListener<ApplicationStartedEvent>{@Override
	public void onApplicationEvent(ApplicationStartedEvent event) {
		log.info("======================");
		log.info("						started						");
		log.info("======================");
	}
}
