package whiteship.tmffjtl.inf.third;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 프로파일 설정
 * 특정한 프로파일에서만 특정 빈을 등록하고 싶을 때 사용
 * 
 * 사용한 파일 : application-prod.properties, application-proddb.properties, banner.txt.backup
 * */
@SpringBootApplication
public class SpringinitApplication {
		
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SpringinitApplication.class);
		app.setWebApplicationType(WebApplicationType.NONE);
		app.run(args);
	}
}
