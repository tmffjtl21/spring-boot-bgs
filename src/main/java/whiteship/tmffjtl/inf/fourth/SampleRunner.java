package whiteship.tmffjtl.inf.fourth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SampleRunner implements ApplicationRunner{
	
	@Autowired
	private KeesunProperties keesunProperties;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		log.debug("================");
		log.debug(keesunProperties.getName());
		log.debug(keesunProperties.getFullName());
		log.debug("================");
		
		System.out.println("===================");
		System.out.println(keesunProperties.getName());
		System.out.println(keesunProperties.getFullName());
		System.out.println("===================");
	}
}
