package whiteship.tmffjtl.inf.fourth;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

/**여러개의 properties를 묶어서 하나의 Bean으로 등록가능*/
@Component
@ConfigurationProperties("keesun")	// application.properties에서 keesun으로 시작하는 프로퍼티를 읽어와서 빈으로 등록
@Validated
@Getter
@Setter
public class KeesunProperties {
	
	@NotEmpty
	private String name;
	private int age;
	private String fullName;
	
	@DurationUnit(ChronoUnit.SECONDS)
	private Duration sessionTimeout = Duration.ofSeconds(30);
	private Duration readTimeout = Duration.ofMillis(1000);
}
