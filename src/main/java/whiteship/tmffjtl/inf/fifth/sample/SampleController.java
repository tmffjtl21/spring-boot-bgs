package whiteship.tmffjtl.inf.fifth.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class SampleController {
	
	@Autowired
	private SampleService sampleService;
	
	@GetMapping("/hello")
	public String hello() {
		log.info("holoman");
		System.out.println("skip");
		
		return "hello " + sampleService.getName();
	}
}
